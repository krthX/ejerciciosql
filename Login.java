package org.bryampaniagua.utilidades;

import java.io.Console;
import java.util.IllegalFormatException;

import org.bryampaniagua.conexiondb.Conexion;
import org.bryampaniagua.utilidades.ingresar.Ingresar;
import org.bryampaniagua.manejador.ManejadorAdministrador;
import org.bryampaniagua.manejador.ManejadorEmpleado;
import org.bryampaniagua.manejador.ManejadorCliente;

public class Login{
	private Console entrada;
	private boolean comprobado;
	private String nombre, contrasenia;
	private Ingresar ingreso;
	public Login(){
		entrada = System.console();
		comprobado = false;
		ingreso = new Ingresar();
	}
	public void logear(ManejadorAdministrador ma, ManejadorCliente mc, ManejadorEmpleado me){
		System.out.print("Nombre >");
		nombre = ingreso.leer();
		try{
			System.out.print("Contrasenia >");
			contrasenia = new String(entrada.readPassword());
		}catch(IllegalFormatException ife){
			ife.printStackTrace();
		}
		comprobado = ma.comprobar(nombre, contrasenia);
		if(comprobado){
			System.out.println("Manejador Administrador");
		}else{
			comprobado = mc.comprobar(nombre, contrasenia);
			if(comprobado){
				System.out.println("Manejador Cliente");
			}else{
				comprobado = me.comprobar(nombre, contrasenia);
				if(comprobado){
					System.out.println("Manejador Empleado");
				}else{
					System.out.println("Compruebe sus datos");
				}
			}
		}
		
	}
}