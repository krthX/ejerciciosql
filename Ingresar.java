package org.bryampaniagua.utilidades.ingresar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Ingresar{
	private static BufferedReader ingreso;
	private static String dato;
	public Ingresar(){
		ingreso = new BufferedReader(new InputStreamReader(System.in));
	}
	public String leer(){
		try{
			dato = ingreso.readLine();
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
		return dato;
	}
}