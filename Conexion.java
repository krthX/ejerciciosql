package org.bryampaniagua.conexiondb;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;

public class Conexion{
	private Connection conexion;
	private Statement estado;
	public Conexion(){
		try{		
			System.out.println("Cargando conexion...");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("Cargando driver...");
			conexion = DriverManager.getConnection("jdbc:sqlserver://192.168.1.6:1433;databaseName=dbBiblioteca2012442", "Paniagua", "Paniagua");
			System.out.println("Cargando base de datos...");
			estado = conexion.createStatement();
			System.out.println("Creando estado..");
		}catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	public boolean execSentencia(String sentencia){
		boolean resultado = false;
		try{
			resultado = estado.execute(sentencia);
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return resultado;
	}
	public ResultSet execConsulta(String consulta){
		ResultSet datos = null;
		try{
			datos = estado.executeQuery(consulta);
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return datos;
	}
}