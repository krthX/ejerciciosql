package org.bryampaniagua.sistema;

import org.bryampaniagua.utilidades.Login;
import org.bryampaniagua.manejador.ManejadorAdministrador;
import org.bryampaniagua.manejador.ManejadorEmpleado;
import org.bryampaniagua.manejador.ManejadorCliente;

public class Principal{
	public static void main(String[] args){
		Login inicio;
		inicio = new Login();
		inicio.logear(new ManejadorAdministrador(), new ManejadorCliente(), new ManejadorEmpleado());
	}
}