package org.bryampaniagua.beans;

public class beansUsuario{
	private String nombre, contrasenia,nick;
	private int id;
	
	public beansUsuario(){
	
	}
	public beansUsuario(int id, String nombre, String contrasenia){
		this.id = id;
		this.nombre = nombre;
		this.contrasenia = contrasenia;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public void setContrasenia(String contrasenia){
		this.contrasenia = contrasenia;
	}
	public String getContrasenia(){
		return this.contrasenia;
	}
	public void setNick(String nick){
		this.nick = nick;
	}
	public String getNick(){
		return this.nick;
	}
}